/*

backorama.exe <file input> <destination>

*/
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <Windows.h>
#include <filesystem>

#define STOP std::cin.get();

namespace fs = std::filesystem;

void main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "backorama.exe <file input> <destination>" << std::endl;
		return;
	}

	std::string file_in(argv[1]), destination_pre(argv[2]);
	
	time_t rawtime = time(0);
	struct tm timeinfo;
	char buffer[80];

	localtime_s(&timeinfo,&rawtime);

	strftime(buffer, sizeof(buffer), "%d-%m-%y__%H-%M-%S", &timeinfo);
	std::string str(buffer);
	
	destination_pre += "\\Backup__"+str+"\\";

	std::ifstream file(file_in);
	std::map<std::string,std::string> lines;

	if (file.is_open()) {
		std::string line;
		while (getline(file, line)) {
			size_t pos = 0;
			std::string token;
			while ((pos = line.find("::$$::")) != std::string::npos) {
				token = line.substr(0, pos);
				lines[token] = "";
				line.erase(0, pos + std::string("::$$::").length());
			}
			lines[token] = line;
		}
		file.close();
	}

	//std::cout << destination << std::endl;
	CreateDirectory(destination_pre.c_str(), NULL);

	for (std::map<std::string, std::string>::iterator iter = lines.begin(); iter != lines.end(); ++iter)
	{
		std::string sc = iter->second, fc = iter->first, fn;
		size_t pos = 0;
		std::string token;
		std::string trail = destination_pre;


		try // If you want to avoid exception handling then use the error code overload of the following functions.
		{
			fs::create_directories(destination_pre + sc); // Recursively create target directory if not existing.
			if (fc.back() == '*')
			{
				fc.pop_back();
				std::cout << "Backing All files in " + fc + " too " + destination_pre + sc << std::endl;
				for (const auto & entry : fs::directory_iterator(fc))
				{
					std::cout << "		>>>>" << fs::path(entry.path()).filename();
					fs::copy(entry, destination_pre + sc, fs::copy_options::recursive);
					std::cout << "	Done!" << std::endl;
				}
			}
			else
			{
				bool fd = false;
				if (fc.back() == '\\')
				{
					fd = true;
					fc = fc.substr(0, fc.size() - 1);
				}
				fs::path f(fc);
				std::cout << "Backing " << (fd ? " Folder " : " File ") << fc + (fd ? "\\" : "") << " to " << destination_pre + sc + f.filename().string();
				fs::copy(fc, destination_pre + sc + f.filename().string() + (fd?"\\":""), fs::copy_options::recursive);
				std::cout << "	Done!" << std::endl;
			}
			
		}
		catch (std::exception& e) // Not using fs::filesystem_error since std::bad_alloc can throw too.
		{
			std::cout << e.what();
		}


	}
	std::cout << "Completed, press enter to quit." << std::endl;
	STOP
	return;
}